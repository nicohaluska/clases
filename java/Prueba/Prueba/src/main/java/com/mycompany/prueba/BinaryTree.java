package com.mycompany.prueba;
import java.util.List;
import java.util.LinkedList;

public class BinaryTree<T extends Comparable<T>> { // T debe de implementar Comparable (Se puede comparar)
    T worth;
    BinaryTree<T> leftChild;
    BinaryTree<T> rightChild;
    // private BinaryTree<T> inicial;

    public BinaryTree(T valor) {
	this.worth = valor;
	this.leftChild = null;
	this.rightChild = null;
    }

    public T getdata() {
	return worth;
    }

    public void setdata(T valor) {
	this.worth = valor;
    }

    public BinaryTree<T> getLeftChild() {
	return leftChild;
    }

    public BinaryTree<T> getRightChild() {
	return rightChild;
    }

    public void addLeftChild(T valor) {
	if (this.leftChild == null) {
	    this.leftChild = new BinaryTree<T>(valor);
	}
    }

    public void addRightChild(T valor) {
	if (this.rightChild == null) {
	    this.rightChild = new BinaryTree<T>(valor);
	}
    }

    public void removeLeftChild() {
	this.leftChild = null;
    }

    public void removeRightChild() {
	this.rightChild = null;
    }

    public boolean isEmpty() {
	if (worth == null)
	    return true;
	return false;
    }
    public boolean isLeaf() {
	if (this.leftChild == null && this.rightChild == null) {
	    return true;
	    }
	// else if (this.leftChild.getdata() == null && this.rightChild.getdata() == null) {
	//     return true;
	//     }
	return false;
	// return (this.leftChild == null && this.rightChild == null) ? true : false;
    }

    public void insertar(T valor) {
	if (this.worth == null) {
	    this.worth = valor;
	    // this.leftChild = new BinaryTree<>(null);
	    // this.rightChild = new BinaryTree<>(null);
	} else {
	    if (valor.compareTo(this.worth) < 0) { // Not equal, equal returns zero
		if (this.leftChild == null) {
		    this.leftChild = new BinaryTree<T>(valor);
		} else {
		    this.leftChild.insertar(valor);
		}
	    } else if (this.rightChild == null) {
		this.rightChild = new BinaryTree<T>(valor);
	    } else {
		this.rightChild.insertar(valor);
	    }
	}
    }

    public BinaryTree<T> espejo() {
	if (this.isLeaf() != true) {
	    BinaryTree<T> tmp = this.leftChild;
	    this.leftChild = this.rightChild;
	    this.rightChild = tmp;
	    this.leftChild.espejo();
	    this.rightChild.espejo();
	}
	return this;
    }

    public int contarHojas(BinaryTree<T> nodo) {
	if (nodo == null) {
	    return 0;
	}
	if (nodo.isLeaf()) {
	    return 1;
	}
	int hojasIzquierda = contarHojas(nodo.getLeftChild());
	int hojasDerecha = contarHojas(nodo.getRightChild());
	return hojasIzquierda + hojasDerecha;
    }

    public void entreNiveles (int n, int m) {
	if (this.isLeaf()) {
	    return;
	    }
	List<BinaryTree> cola = new LinkedList<BinaryTree>();
	cola.add(this);
	while (cola.isEmpty() != true){
	    System.out.println("a" + cola.getFirst().getdata());
	    if (cola.getFirst().getLeftChild() != null) {
		cola.add(cola.getFirst().getLeftChild());
	    }
	    if (cola.getFirst().getRightChild() != null) {
		cola.add(cola.getFirst().getRightChild());
	    }
	    cola.removeFirst();
	}
    }
}
