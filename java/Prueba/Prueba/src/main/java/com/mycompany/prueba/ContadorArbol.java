package com.mycompany.prueba;

import java.util.List;

public class ContadorArbol {
    public List<Integer> numerosPares(BinaryTree<Integer> node, List<Integer> list) {
	if (node.getLeftChild() != null && node.getLeftChild().getdata() != null) {
	    numerosPares(node.getLeftChild(), list);
	    }
	if (node.getdata() % 2 == 0) {
		list.add(node.getdata());
	    }
	if (node.getRightChild() != null && node.getRightChild().getdata() != null) {
	    numerosPares(node.getRightChild(), list);
	    }
	return list;
    }
}
