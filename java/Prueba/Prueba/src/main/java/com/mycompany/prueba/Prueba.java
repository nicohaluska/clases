package com.mycompany.prueba;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class Prueba<T extends Comparable<T>> {

    public int contarHojas() {
	return inicial.contarHojas(inicial);
	// throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    BinaryTree<T> inicial;

    public Prueba(T data) {
	this.inicial = new BinaryTree<>(data);
    }

    public void print(StringBuilder buffer, String prefix, String childrenPrefix, BinaryTree<T> node) {
	buffer.append(prefix);
	buffer.append(node.getdata());
	buffer.append('\n');
	List<BinaryTree<T>> list = new ArrayList<BinaryTree<T>>();
	if (node.getLeftChild() != null && node.getLeftChild().getdata() != null ) {
	    list.add(node.getLeftChild());
	}
	if (node.getRightChild() != null && node.getRightChild().getdata() != null ) {
	    list.add(node.getRightChild());
	}
	for (Iterator<BinaryTree<T>> it = list.iterator(); it.hasNext();) {
	    BinaryTree<T> next = it.next();
	    if (it.hasNext()) {
		print(buffer, childrenPrefix + "├── ", childrenPrefix + "│   ", next);
	    } else {
		print(buffer, childrenPrefix + "└── ", childrenPrefix + "    ", next);
	    }
	}
    }

    public static void main(String[] args) {
	Prueba<Integer> prueba = new Prueba<>(null);
	prueba.inicial = new BinaryTree<>(null); // Initialize the BinaryTree
	prueba.inicial.insertar(6);
	prueba.inicial.insertar(4);
	prueba.inicial.insertar(5);
	prueba.inicial.insertar(3);
	prueba.inicial.insertar(8);
	prueba.inicial.insertar(7);
	prueba.inicial.insertar(9);

	// System.out.println("Los numeros en Preorden");
	// prueba.preorden(prueba.inicial);
	System.out.println();
	int hojas = prueba.contarHojas();
	System.out.println("La cantidad de hojas que tiene el arbol es: " + hojas);
	StringBuilder tree = new StringBuilder(100);
	prueba.print(tree, "", "", prueba.inicial);
	System.out.print(tree);

	tree = new StringBuilder(100);
	prueba.inicial.espejo();
	prueba.print(tree, "", "", prueba.inicial);
	System.out.print(tree);


	// ContadorArbol contador = new ContadorArbol();
	// contador.numerosPares(prueba.inicial, evenList);
	// System.out.println("Pares:" + evenList);

	RedBinariaLlena retardo = new RedBinariaLlena();
	System.out.println("" + retardo.retardoReenvio(prueba.inicial));

	prueba.inicial.entreNiveles(1,1);
    }
}
