package com.mycompany.prueba;

public class RedBinariaLlena {
    public int retardoReenvio(BinaryTree<Integer> nodo) {
	int retardo = nodo.getdata();
	int leftValue = 0;
	int rightValue = 0;
	if (nodo.getLeftChild() != null) {
	    leftValue = retardoReenvio(nodo.getLeftChild());
	    }
	if (nodo.getRightChild() != null) {
	    rightValue = retardoReenvio(nodo.getRightChild());
	    }
	return (retardo + Math.max(leftValue, rightValue));
    }
}
