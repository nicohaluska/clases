program SimpleLinkedList;

type			//definimos un tipo custom
    StudentData= record
		     Name    : string;
		     ID	     : integer;
		     Age     : integer;
		     Average : real;
		 End;
    Node= ^NodeRec;
    NodeRec= record
		 Data : StudentData;
		 Next : Node;
	     End;     


var
    Head, Current, NewNode : Node; //Head es el ultimo nodo creado no se toca, current es para las operaciones con nodos ya creados
    i, rmId		   : Integer;
    command		   : string;
    NewData		   : StudentData;
    Running		   : Boolean;

Procedure addStudent(data : StudentData; var Head : Node); // el primer argumento es solo un valor, el segundo es por referencia
var NewNode : Node;
    begin
	New(NewNode);
	NewNode^.Data := data; //Node es un puntero a una estructura, por lo que hay que usar el ^
	NewNode^.Next := Head;
	Head := NewNode;
    end;

Function searchId(id : integer; Head : Node):Node;
var Current : Node; ptr : ^word;
    begin	
	Current	:= Head;
	ptr := addr(Current);
	WriteLn(ptr^);
	while Current <> nil do
	begin
	    if Current^.Data.ID = id then
	    begin
		WriteLn(Current^.Data.ID);
		WriteLn(Current^.Data.Name);
		exit(Current);
	    end
	    else
		Current := Current^.Next;
	end;
	searchID:=nil;
    end;

Procedure rmStudent(id : integer; var Head : Node);
var Current, Previous : Node;
    begin
	Current := Head;
	Previous := nil;
	while Current <> nil do
	begin
	    Writeln(Current^.Data.ID);
	    if Current^.Data.ID = id then
	    begin
		if Previous = nil then //Si es el ultimo
		    Head := Current^.Next //Muevo head
		    {NOTA: para el else, no se le pone ; al final de la anterior linea}
		else
		    Previous^.Next := Current^.Next; //Sino solo cambio la referencia del anterior
		
		Dispose(Current);
		Exit();
	    end;
	    Previous := Current;
	    Current := Current^.Next;
	end;
    end;

Procedure printList(Head : Node);
var Current : Node; ptr : ^word;
begin
    { Print the elements of the linked list }
    Current := Head; //Me paro en el ultimo
    while Current <> nil do // Mientras current no sea nil
    begin
	WriteLn(Current^.Data.Name, ' '); //Imprime puntero a current.data
	ptr:=addr(Current^.Next); //No se pueden imprimir punteros directamente, asi que primero pedimos un puntero al puntero del proximo
	WriteLn(ptr^); //Despues imprimimos lo que apunta ese puntero.
	Current := Current^.Next; //Se mueve al siguiente
    end;
end;

Function calculateAverage(Head : Node):real;
var Current : Node; avgSum : real; counter : integer; 
begin
    counter:=0; //Inicializo variables
    avgSum:=0;
    Current := Head; //Me paro en el ultimo
    while Current <> nil do // Mientras current no sea nil
    begin
	counter:= counter+1; //Incremento contador
	avgSum := avgSum+Current^.Data.Average; //Le sumo al promedio
	Current := Current^.Next; //Se mueve al siguiente
    end;
    calculateAverage := avgSum/counter;
end;

begin
    Head := nil; //Setea head en nil
    Running := true;

    while Running do
	begin
	    Readln(command);

	    if command = 'add' then
	    begin //Necesario para ifs de mas de una linea, sino va solo el "then"
		WriteLn('Student name');
		Readln(NewData.Name);
		WriteLn('Student ID');
		Readln(NewData.ID);

		if searchId(NewData.ID, Head) <> nil then 
		begin
		    WriteLn('Student ID already exists');
		    Continue; //Finish actual cycle, continues the next loop
		end;

		WriteLn('Student age');
		Readln(NewData.Age);
		WriteLn('Student average');
		Readln(NewData.Average);
		addStudent(NewData, Head);
	    end //sin ; para el else

	    else if command = 'rm' then
	    begin
		WriteLn('Which ID to delete?');
		Readln(rmId);
		rmStudent(rmId, Head);
	    end

	    else if command = 'avg' then
		WriteLn('Total avg: ', calculateAverage(Head):0:2 ) //Solo dos decimales

	    else if command = 'ls' then
		printList(Head)

	    else if command = 'exit' then
		Running := false

	    else
		begin
		    WriteLn('help');
		    WriteLn('Write ls to list all students');
		    WriteLn('Write add to add a student');
		    WriteLn('Write rm to delete a student by ID');
		    WriteLn('Write avg to get the average of all student''s averages');
		end;
	end;

  { Create a simple linked list with values 1 to 5 }
  for i := 1 to 5 do
  begin
    New(NewNode); //Reserva memoria para un nodo llamado "NewNode"
    { NewNode^.Data := i; //Setea el dato en el index del for }
    NewNode^.Next := Head; //Setea el puntero al proximo en el anterior
    Head := NewNode; //Setea el head en el actual
  end;

   {NOTA: la linked list se crea invertida}

  { Clean up the memory allocated for the linked list }
  Current := Head;
  while Current <> nil do
  begin
    NewNode := Current;
    Current := Current^.Next;
    Dispose(NewNode);
  end;
end.
