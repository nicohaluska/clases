#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct studentData{
  int id;
  char *name;
  int age;
  float average;
};
struct Node{
  struct studentData Data;
  struct Node* Next;
};


struct Node* allocateNode(){
  struct Node* newNode = malloc(sizeof(struct Node));
  return newNode;
}
struct studentData getData(){
  struct studentData Data;
  printf("Input student's ID\n");
  scanf("%d", &Data.id);
  printf("Input student's name\n");
  scanf("%ms", &Data.name); //ms tag allocates the string and sets the pointer automatically
  printf("Input student's age\n");
  scanf("%d", &Data.age);
  printf("Input student's average\n");
  scanf("%f", &Data.average);
  return Data;
}

struct Node* createNode(struct Node** Head, struct studentData Data){
  struct Node* newNode = allocateNode();
  newNode->Next = *Head;
  newNode->Data = Data;
  *Head = newNode;
  return newNode;
}

void createSet(struct Node** Head){
  for (int i=0; i<10; i++){
    struct studentData data;

    if(*Head != NULL){
      data.id = (*Head)->Data.id + i;
    }
    else{
      data.id=i;
    }

    int nameMem = snprintf(NULL, 0, "pepe%d", i)+1;
    char *name = (char*)malloc(nameMem);
    snprintf(name, nameMem, "pepe%d", i);
    data.name = name;
    data.age=rand()%10 + 10;
    data.average=rand()%100/10.0;
    createNode(Head, data);
  }
}

void printStudentData(struct studentData data){
  printf("%d\n", data.id);
  printf("%s\n", data.name);
  printf("%d\n", data.age);
  printf("%f\n", data.average);
}

void printAll(struct Node* Head){
  struct Node* Current=Head;
  while (Current){
    printStudentData(Current->Data); 
    Current = Current->Next;
  }
}


struct Node* Head = NULL;
char *command;
int main(){
  while (0){
    scanf("%ms", command);
  }
  createSet(&Head);
  printAll(Head);
  return 0; 
}

