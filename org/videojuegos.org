#+author: Valentino Slavkin
#+title:Desarrollo de videojuegos
* Que es un videojuego
[[https://en.wikipedia.org/wiki/Video_game][Wikipedia (ingles)]] [[https://es.wikipedia.org/wiki/Videojuego][Wikipedia (castellano)]]
Un videojuego o juego de video es un software o juego electrónico en el que uno o más jugadores interactúan por medio de un controlador (teclado, joystick, sensor de movimiento, etc) para generar feedback visual de un display, usualmente una pantalla o unos auriculares.

Los primeros prototipos aparecieron en los 50s y 60s, pero, el primer videojuego de consumo fue "Computer Space" en 1971. Un año después aparece pong, y la primera consola de videojuegos.

#+DOWNLOADED: https://upload.wikimedia.org/wikipedia/commons/5/50/Tennis_For_Two_on_a_DuMont_Lab_Oscilloscope_Type_304-A.jpg @ 2024-01-18 16:26:41
#+attr_html: :width 500px
[[file:Que_es_un_videojuego/2024-01-18_16-26-41_Tennis_For_Two_on_a_DuMont_Lab_Oscilloscope_Type_304-A.jpg]]




# Local Variables:
# jinx-languages: "es_AR en_US-large"
# End:
* Game development (gamedev)
Es el proceso de crear un videojuego. Es un proceso multidisciplinario, que involucra programación, diseño, arte 2D y 3D, audio, interfaz de usuario y escritura.
** Complejidad
Con el paso del tiempo, la complejidad de los videojuegos fue aumentando exponencialmente. En los 80s, un solo programador podía desarrollar un videojuego completo. Para la [[https://en.wikipedia.org/wiki/Home_video_game_console_generations#Second_generation_(1976%E2%80%931992)][segunda]] (1976-1983)[Atari 2600, Game & Watch] y [[https://en.wikipedia.org/wiki/Home_video_game_console_generations#Third_generation_(1983%E2%80%932003)][tercera]] (1983-2003)[Famicom/NES, Master system, Atari 7800] generación de consolas, con la creciente popularidad de los graficos en 3D, y las mayores expectativas para las visuales y calidad, se hizo cada vez más difícil para una sola persona producir un videojuego mainstream. El costo promedio de desarrollar un videojuego "high end" (AAA) subio de $1-4 Millones hasta mas de $200 millones en 2023. Pero al mismo tiempo el desarrollo de videojuegos independientes floreció.
** Motores de videojuegos (Game engines)
Con la creciente popularidad del medio, a lo largo de la historia se desarrollaron varios motores de videojuegos. Los motores de videojuegos son básicamente frameworks diseñados principalmente para el desarrollo de los mismos, que contienen muchas de las herramientas necesarias, como por ejemplo: motor de renderizado para 2D y/o 3D, un motor de físicas, herramientas de sonido, animacion, scripting, inteligencia artificial, redes, manejo de memoria, threading, etc.
Al principio, los motores eran más simples, y muchas veces específicos para cierto tipo/género de videojuegos. Pero hoy en día también existen motores "generales" que, en teoría, tienen las herramientas necesarias para desarrollar cualquier tipo/género de videojuego.
*** Opciones populares
**** Unreal Engine
La opción más popular en estudios AAA, es el más complejo, pero también el que tiene más herramientas. No tan popular en videojuegos indie, debido a su alta complejidad. Se programa utilizando C++.
Tambien muy usado en arquitectura
**** Unity
La opción más popular en general, es por lejos la mas utilizada en videojuegos móviles e indie. Es medianamente simple, se considera como el motor más versatil. Se programa utilizando C#
**** Godot
El motor open-source más popular, con muchas menos herramientas que los dos anteriores, pero más simple, y con 2D verdadero. Se puede programar en varios lenguajes, como gdscript, C#, C++, etc.
[[https://www.gdquest.com/][Tutoriales]]

