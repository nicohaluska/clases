#+title: Haciendo una interfaz de consola en python
#+author: Valentino Slavkin

* Input
En python, existe la funcion input() que devuelve el texto que escriba el usuario en la consola.
Puede recibir un argumento opcional, que se imprimira justo antes del input.
#+begin_src python
  x = input("Escriba un numero")
  print(x)
#+end_src

#+RESULTS:

* Implementando una interfaz de consola
** Utilizando if y elses
Podemos hacer una interfaz muy basica utilizando if's y else's de la siguiente manera:
#+begin_src python
  comando = input("Ingrese el comando")
  if comando == "1":
    #comando 1
  elif comando == "agregar":
      #comando agregar
  elif comando == "x":
      #comando x
  #etc
#+end_src

** Utilizando diccionarios
Otra manera de hacer la interfaz es con diccionarios:
#+begin_src python
  comandos={"1":func1, "agregar":funcAgregar, "x":funcX}
  comando = input("Ingrese el comando")
  if comando in comandos:
      comandos[comando]()
  else:
      print("Error, the command doesnt exist")
#+end_src
La desventaja es que hay que hacer funciones para cada una de las funciones, pero se logra una interfaz mucho mas versatil.

*** Ayuda
Por ejemplo podemos agregar campos al diccionario para tener un comando de ayuda
#+begin_src python
  comandos={"1":func1, "agregar":funcAgregar, "x":funcX, "help":print(comandos)}
  comando = input("Ingrese el comando")
  if comando in comandos:
      comandos[comando]()
  else:
      print(f"{comando} no se encontro")
#+end_src
Pero ahi hay un problema, no podemos poner una funcion dentro del diccionario, solo el nombre de la misma.
Para solucionarlo, podemos seguir dos caminos:
- El primero es utilizando lambdas:
#+begin_src python
     comandos={"1":func1,
               "agregar":funcAgregar,
               "x":funcX,
               "help":lambda: print(comandos)}

     comando = input("Ingrese el comando")
     if comando in comandos:
         comandos[comando]()
     else:
         print(f"{comando} no se encontro")
#+end_src
- El segundo utilizando diccionarios 
#+begin_src python
     comandos={"1":func1,
               "agregar":funcAgregar,
               "x":{"func":funcX, "argumentos":[]},
               "help":{"func":print, "argumentos":[]}}

     comando = input("Ingrese el comando")
     if comando in comandos:
         comandos[comando]()
     else:
         print(f"{comando} no se encontro")
#+end_src
