import networkx as nx
import matplotlib.pyplot as plt
import tkinter as tk #Para poder abrir ventanas
from PIL import ImageTk #para mostar imagenes
import json
import os

from networkx.readwrite import json_graph



#################################################################################

# Variables globales
status = {
    "carga": 0,  # Establece la carga inicial
    "nave": "X-wing",
    "distancia": 0,  # Añade una entrada para la distancia
    "nodo": "Tatooine",
}
G = nx.Graph()
window = tk.Tk()
tkimage=0
img_label=tk.Label(window)

#################################################################################

naves = {
    "X-wing": {"carga": 100, "multiplicador": 0.5},
    "TIE":    {"carga": 200, "multiplicador": 1  },
    "Falcon": {"carga": 300, "multiplicador": 3  },
}

def multiplicar_distancia(distancia, multiplicador):
    global status
    status["distancia"] = status["distancia"] + (distancia * multiplicador)

#################################################################################

def abrir_json(archivo):
   with open(archivo, 'r') as grafo_file:
       grafo = json.load(grafo_file)
   return grafo

def seleccionar_archivo():
    while True:
        archivo=input("Escriba el nombre del archivo")
        if os.path.isfile(archivo):
            return archivo
        else:
            print("No se encuentra el archivo")
            
def dibujar_grafo(grafo):
    global tkimage, img_label

    A = nx.nx_agraph.to_agraph(grafo)  # convert to a graphviz graph
    for nodo in A.iternodes():
        if nodo == status["nodo"]:
            nodo.attr['color'] = 'red'
        else:
            nodo.attr['color'] = 'black'
    print(A)
            
    A.draw("attributes.png", prog="neato")  # Draw with pygraphviz
    # window.title("hola")
    tkimage = ImageTk.PhotoImage(file="attributes.png")
    img_label.configure(image=tkimage)
    img_label.pack()
    window.update()

def viajar_destino():
    destino = input("ingrese un destino")
    viajar(destino)
def viajar(destino):
    global status
    distancia = nx.shortest_path_length(G, source=status["nodo"], target=destino) * naves[status["nave"]]["multiplicador"]
    print(distancia)
    status["distancia"] += distancia
    status["nodo"] = destino
    # evento(grafo, destino)

#################################################################################

# grafo = {
#     "Tatooine":     {"Endor": 5, "Alderaan": 4                 },
#     "Endor":        {"Alderaan": 8, "Hoth": 10, "Coruscant": 4 },
#     "Alderaan":     {"Naboo": 6                                },
#     "Hoth":         {"Kamino": 7, "Coruscant": 4              },
#     "Naboo":        {"Kashyyyk": 8                             },
#     "Kamino":       {"Mustafar": 10                            },
#     "Kashyyyk":     {"Kamino": 9, "Geonosis": 2, "Tierra": 1   },
#     "Mustafar":     {"Dagobah": 6, "Geonosis": 6               },
#     "Geonosis":     {"Dagobah": 7, "Tierra": 8                 },
#     "Dagobah":      {"Jakku": 2                                },
#     "Coruscant":    {"Naboo": 10, "Kashyyyk": 5                },
#     "Tierra":       {"Jakku": 3},
#     "Jakku": {}
# }

#################################################################################

###--- GRAFO CON LINEA ROJA ---###
def encontrar_y_dibujar_camino_mas_corto(G, origen, destino):
    camino_mas_corto = dijkstra(grafo, origen, destino)  # Calcular el camino más corto con Dijkstra

    if len(camino_mas_corto) > 1:
        edges = [(camino_mas_corto[i], camino_mas_corto[i + 1]) for i in range(len(camino_mas_corto) - 1)]

        pos = nx.spring_layout(G)  # Distribución de los nodos en el espacio
        labels = nx.get_edge_attributes(G, 'weight')  # Etiquetas de las distancias

        # Dibujar el grafo con las aristas del camino más corto en rojo
        nx.draw_networkx_nodes(G, pos, node_size=300, node_color='lightblue')
        nx.draw_networkx_labels(G, pos)
        nx.draw_networkx_edges(G, pos)
        nx.draw_networkx_edges(G, pos, edgelist=edges, edge_color='red', width=2.0)
        nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)

        plt.axis('off')  # Desactivar los ejes
        plt.show()
    else:
        print("No se encontró un camino entre los nodos.")

#################################################################################

###--- DIJKSTRA ---###

# Función para encontrar el camino más corto usando el algoritmo de Dijkstra
def dijkstra(grafo, inicio, fin):
    # Inicializar los diccionarios de distancias y padres
    distancias = {}
    padres = {}
    nodos_no_visitados = set(grafo)

    for nodo in nodos_no_visitados:
        distancias[nodo] = float("inf")
    distancias[inicio] = 0

    # Implementación del algoritmo de Dijkstra
    while nodos_no_visitados:
        nodo_actual = min(nodos_no_visitados, key=lambda nodo: distancias[nodo])
        nodos_no_visitados.remove(nodo_actual)

        for vecino, peso in grafo[nodo_actual].items():
            distancia = distancias[nodo_actual] + peso
            if distancia < distancias[vecino]:
                distancias[vecino] = distancia
                padres[vecino] = nodo_actual

    # Reconstruir el camino más corto
    camino = []
    nodo_actual = fin
    while nodo_actual != inicio:
        camino.insert(0, nodo_actual)
        nodo_actual = padres[nodo_actual]
    camino.insert(0, inicio)

    return camino

def custom_dijkstra(graph, source, target):
    # Initialize distances dictionary with infinity for all nodes except the source
    distances = {node: float('inf') for node in graph.nodes()}
    distances[source] = 0

    # Initialize the predecessors dictionary to keep track of the path
    predecessors = {}

    # Create a priority queue (min-heap) to store nodes and their distances
    priority_queue = [(0, source)]

    while priority_queue:
        current_distance, current_node = min(priority_queue)

        # Remove the current node from the priority queue
        priority_queue.remove((current_distance, current_node))

        if current_distance > distances[current_node]:
            continue

        for neighbor in graph[current_node]:
            weight = graph[current_node][neighbor].get("weight", 1)  # Default weight is 1

            distance = current_distance + weight

            if distance < distances[neighbor]:
                distances[neighbor] = distance
                predecessors[neighbor] = current_node
                priority_queue.append((distance, neighbor))

    # Reconstruct the path from source to target
    path = []
    current_node = target
    while current_node in predecessors:
        path.insert(0, current_node)
        current_node = predecessors[current_node]
    if path and path[0] == source:
        path.insert(0, source)

    return path, distances[target]

#################################################################################

# Función para calcular la distancia total del camino
def distancia_total(grafo, camino):
    distancia = 0
    for i in range(len(camino) - 1):
        distancia += grafo[camino[i]][camino[i + 1]]
    return distancia

#################################################################################


def seleccion_nave():
    while True:
        print("-----------------")
        print("1) X-wing T-65: es rápido, la cantidad de distancia se reduce a la mitad, pero lleva poca carga")
        print("2) El TIE Advanced x1: velocidad normal, lleva una carga normal")
        print("3) Millennium Falcon: lento, pero lleva mucha carga")
        eleccion_nave = input("Seleccione una nave utilizando el número correspondiente: ")
        if eleccion_nave.isdigit():
            indice_nave = int(eleccion_nave)
            if 1 <= indice_nave <= len(naves):
                indice_nave -= 1
                nombre_nave = list(naves.keys())[indice_nave]
                print(f"Seleccionaste la nave: {nombre_nave}")
                status["nave"] = nombre_nave  # Asignar la nave seleccionada a status
                print("-----------------")
                return naves[nombre_nave]
            else:
                print("Selección no válida. Por favor, ingrese un número válido para seleccionar una nave.")

#################################################################################

def eventos(camino_mas_corto):
    global status
    if "Tierra" in camino_mas_corto:
        status["nave"]["carga"] -= 20
    if "Coruscant" in camino_mas_corto:
        status["nave"]["carga"] /= 2
    if "Naboo" in camino_mas_corto:
        status["nave"]["multiplicador"] += 2

#################################################################################

#####---TODA OTRA FUNCION DEBE ESTAR ARRIBA DE ESTO---###
comandos = {
    "camino":{"funcion":"camino_corto()", "descripcion":"Busca el camino mas corto entre 2 nodos" },
    "agregar_nodo":{"funcion":"agregar_nodo(obtener_info_nodo())", "descripcion":""},
    "borrar_nodo":{"funcion":"eliminar_nodo()", "descripcion":""},
    "editar_nodo":{"funcion":"editar_nodo()", "descripcion":""},
    "guardar":{"funcion":"guardar_json()", "descripcion":""},
    "cargar":{"funcion":"abrir_json()", "descripcion":""},
    "viajar":{"funcion":"viajar_destino()", "descripcion":""},
    "ayuda":{"funcion":"imprimir_comandos()", "descripcion":""},
}
def imprimir_comandos():
    for i in comandos:
        print(i)
        print(comandos[i]["descripcion"])

def eliminar_nodo():
    print(G.nodes)
    nodo = input("Elija el nodo")
    # del grafo[nodo]
    G.remove_node(nodo)

def obtener_info_nodo():
    nodo={}
    nombre = input("Elija el nombre")
    nodo["id"]=nombre
    links = {}
    while True:
        nombreLink = input("ingrese un link, ingrese -1 para terminar")
        distancia = input("Ingrese la distancia, ingrese -1 para terminar")
        if nombreLink == '-1' or distancia == '-1':
            break
        else:
            links[nombreLink] = distancia
    nodo["links"] = links
    return nodo
    # nodo["evento"] = {funcion}
    
def agregar_nodo(nodo):
    info_nodo = nodo#obtener_info_nodo()
    G.add_node(info_nodo["id"])
    for link in info_nodo["links"]:
        G.add_edge(info_nodo["id"], link, weight=info_nodo["links"][link])

def editar_nodo():
    info_nodo = obtener_info_nodo()
    G.remove_node(info_nodo["id"])
    agregar_nodo(info_nodo)
    
# Menú para interactuar con el usuario
def camino_corto():
    planetas = list(grafo.keys())
    print(planetas)
    origen = input("Nodo de origen: ")
    destino = input("Nodo de destino: ")
    
    if origen in grafo and destino in grafo:
        camino_mas_corto = dijkstra(grafo, origen, destino)
        if len(camino_mas_corto) > 1:
            print(f"Camino más corto: {' -> '.join(camino_mas_corto)}")
            distancia = distancia_total(grafo, camino_mas_corto)
            
            # Llama a eventos(camino_mas_corto) para verificar y ajustar la carga
            eventos(camino_mas_corto)
            
            # Aplicar el multiplicador de velocidad después de eventos
            distancia *= status["nave"]["multiplicador"]
            
            # Actualizar la carga final en status
            status["carga"] = status["nave"]["carga"]
            
            print(f"Distancia total: {distancia}")
            print("Tu carga final fue de:", status["carga"])
            
            encontrar_y_dibujar_camino_mas_corto(G, origen, destino)
            input("Presione enter...")
            
        else:
            print("No se encontró un camino entre los nodos.")
    else:
        print("Nodos no válidos. Por favor, ingrese nodos válidos.")
                
def menu():
    print("\nMenú:")
    eval(comandos["ayuda"]["funcion"])
    comando = input("Seleccione una opción: ")
    if comando in comandos:
        eval(comandos[comando]["funcion"])
    else:
        print("Opción no válida. Por favor, seleccione una opción válida.")

def main():
    global status
    global grafo
    global G
    seleccion_nave()
    archivo = seleccionar_archivo()
    grafo = abrir_json(archivo)
    # Crear el grafo
    G = nx.Graph()
    G = json_graph.node_link_graph(grafo)
    status["carga"] = naves[status["nave"]]["carga"]
    print(set(G))
    while True:
        # instanciar_nodos(grafo)
        dibujar_grafo(G)
        menu()

if __name__ == "__main__":
    main()
