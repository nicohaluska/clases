import networkx as nx
import pygraphviz as pgv
# Definir los nodos
# nodos = ["Tatooine", "Endor", "Alderaan", "Hoth", "Coruscant", "Naboo", "Kamino", "Kashyyyk", "Mustafar", "Geonosis", "Tierra", "Dagobah", "Jakku", "fin del juego"]

# Inicializar el grafo con distancias
grafo = {
        "Tatooine":     {"links":{"Endor": 10, "Alderaan": 10                 },},
           "Endor":     {"links":{"Alderaan": 10, "Hoth": 10, "Coruscant": 10 },},
        "Alderaan":     {"links":{"Naboo": 10                                 },},
            "Hoth":     {"links":{"Kamino": 10, "Coruscant": 10               },},
       "Coruscant":     {"links":{"Naboo": 10, "Kashyyyk": 10                 },},
           "Naboo":     {"links":{"Kashyyyk": 10                              },},
          "Kamino":     {"links":{"Mustafar": 10                              },},
        "Kashyyyk":     {"links":{"Kamino": 10, "Geonosis": 10, "Tierra": 10  },},
        "Mustafar":     {"links":{"Dagobah": 10, "Geonosis": 10               },},   
        "Geonosis":     {"links":{"Dagobah": 10, "Tierra": 10                 },},
          "Tierra":     {"links":{"Jakku": 10                                 },},
         "Dagobah":     {"links":{"Jakku": 10                                 },},
           "Jakku":     {"links":{"fin del juego": 0                          },}
}
naves = {
    "X-wing T-65":       {"carga":10  },
    "TIE Advanced x1":   {"carga":20  },
    "Millennium Falcon": {"carga":30  },
}

def imprimir_introduccion():
    print("Este es el planeta en el que apareciste. Su nombre es", nodo_actual)
    print("selecciones una nave para comenzar el viaje")
    #------------------------------------------
    print("-----------------------")
    print("las naves para el viaje son 3: X-wing T-65, El TIE Advanced x1 y Millennium Falcon")
    print("Cada nave tiene sus carcteristicas:" )
    print("1) X-wing T-65: es rápido la cantidad de distancia se reduce a la mitad, pero lleva poca carga")
    print("2) El TIE Advanced x1: velocidad normal, lleva una carga normal")
    print("3) Millennium Falcon: lento, pero lleva mucha carga")

def seleccion_nave():
    while True:
        print(list(naves))
        eleccion_nave = input("Seleccione una con los números respectivos: ")
        indice_nave = list(naves)[int(eleccion_nave)]
        if naves[indice_nave]:
            print("Seleccionaste la nave ", indice_nave)
            nave_actual = naves[indice_nave]
            return nave_actual
        else:
            print("Selección no válida. Por favor, ingresa un número válido para seleccionar una nave.")
            print("--------------------")

def imprimir_datos_nodo(nodo):
    print("Estas en", nodo)
    opciones_disponibles = list(grafo[nodo].keys())
    print("Sus opciones de viaje son", opciones_disponibles)

def seleccionar_destino():
    print("Ingrese el nombre del planeta al que desea viajar:")
    nuevo_destino = input("Destino: ")
    if nuevo_destino in grafo[nodo_actual]["links"]:
        return nuevo_destino
    else:
        print("-------------")
        print("Destino no válido. Por favor, ingrese un destino válido.")
        print("-------------")

def viajar(actual, destino, distancia):
        distancia += grafo[actual]["links"][destino]
        actual = destino
        print("-------------")

def main():
    # Inicio
    nodo_actual = "Tatooine"
    distancia_total = 0
    imprimir_introduccion()
    nave_actual = seleccion_nave()
    carga = nave_actual["carga"]
    #------------------------------------------
    
    while True:
        imprimir_datos_nodo(nodo_actual)
        print("Carga restante:", carga)
        destino = seleccionar_destino()
        viajar(nodo_actual, destino, distancia_total)
        
        
        # if nodo_actual == "Tierra": 
        #     print("Has pasado por la Tierra mas específicamente por Argentina. Te robaron 20 cargamentos. Te quedan:")
        #     carga -= 20
        
        # elif nodo_actual == "Coruscant":
        #     print("pasaste por Coruscant. El Imperio te confisco la mitad de tu cargamento. Puedes seguir")
        #     carga = carga/2
        
        # elif nodo_actual == "Jakku":
        #     print("Felicidades llegaste al final de tu ruta espacial. Estos son tus datos")
        #     print("Tu carga total fue de", carga)
        
        # if eleccion_nave == 1:
        #     print("La distancia que recorriste es de", distancia_total / 2)
        # elif eleccion_nave == 2:
        #     print("La distancia que recorriste es de", distancia_total)
        # elif eleccion_nave == 3:
        #     print("La distancia que recorriste es de", distancia_total)
        
        # if nodo_actual == "fin del juego":
        #     break

if __name__=="__main__":
    main()
