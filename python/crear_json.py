import networkx as nx
from networkx.readwrite import json_graph
import json

grafo_file = open("galaxia.json")
grafo = json.load(grafo_file)
grafo_file.close
G = nx.Graph()
for nodo in grafo:
    G.add_node(nodo, descripcion=grafo[nodo]["description"], evento=grafo[nodo]["evento"])
    for link in grafo[nodo]["links"]:
        print(link)
        G.add_edge(nodo, link, weight=grafo[nodo]["links"][link])
raw_json = json_graph.node_link_data(G)
json_formatted = json.dumps(raw_json, indent=2)
print(json_formatted)
with open("grafo.json", "w") as outfile:
    outfile.write(json_formatted)
