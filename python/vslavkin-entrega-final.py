import networkx as nx
from networkx.algorithms.bipartite import color
import tkinter as tk #Para poder abrir ventanas
from PIL import ImageTk #para mostar imagenes
import json
import os

# Crear un grafo en NetworkX
G = nx.Graph()
# Variables globales
status = {
    "nodo": "Tatooine",
    "distancia": 0.0,
    "carga": 0,  # Establece la carga inicial
    "nave":"X-wing",
}
window = tk.Tk()
tkimage=0
img_label=tk.Label(window)

# Definir las naves y el grafo
naves = {
    "X-wing":          {"carga": 100, "multiplicador":0.5},
    "TIE":      {"carga": 200, "multiplicador":1},
    "Falcon":    {"carga": 300, "multiplicador":2},
}

def multiplicar_distancia(*args):
    global status
    status["distancia"] *= args[0]

def seleccionar_archivo():
    while True:
        archivo=input("Escriba el nombre del archivo")
        if os.path.isfile(archivo):
            return archivo
        else:
            print("No se encuentra el archivo")

def abrir_json(archivo):
    grafo_file = open(archivo)
    grafo = json.load(grafo_file)
    grafo_file.close()
    return grafo

def instanciar_nodos(grafo):
    # Agregar nodos y aristas al grafo con distancias
    for planeta, destinos in grafo.items():
        G.add_node(planeta)
        for destino, distancia in destinos["links"].items():
            G.add_edge(planeta, destino, distance=distancia)

# def calcular_rutas():
#     # Calcular la ruta más corta usando el algoritmo de Dijkstra
#     shortest_path = nx.shortest_path(G, source="Tatooine", target="Jakku", weight="distance")
#     shortest_distance = nx.shortest_path_length(G, source="Tatooine", target="Jakku", weight="distance")

def dibujar_grafo(grafo):
    global tkimage, img_label
    # Dibujar el grafo
    for nodo in grafo:
        if nodo == status["nodo"]:
            G.add_node(nodo, color="red")
        else:
            G.add_node(nodo, color="black")
        for link in grafo[nodo]["links"]:
            G.add_edge(nodo,link)
    # pos = nx.spring_layout(G)
    # labels = {nodo: nodo for nodo in G.nodes()}  # Etiquetas de nodos
    # distances = nx.get_edge_attributes(G, 'distance')
    # nx.draw(G, pos, with_labels=True, node_size=500, node_color='skyblue', font_size=12, font_color='black', font_weight='bold')
    # nx.draw_networkx_labels(G, pos, labels=labels)  # Dibujar etiquetas de nodos
    # nx.draw_networkx_edge_labels(G, pos, edge_labels=distances)  # Dibujar etiquetas de distancias
    A = nx.nx_agraph.to_agraph(G)  # convert to a graphviz graph
    A.draw("attributes.png", prog="neato")  # Draw with pygraphviz
    # window.title("hola")
    tkimage = ImageTk.PhotoImage(file="attributes.png")
    img_label.configure(image=tkimage)
    img_label.pack()
    window.update()

def imprimir_introduccion():
    print("Plaenta inicial", status["nodo"])
    print("1) X-wing T-65: es rápido, la cantidad de distancia se reduce a la mitad, pero lleva poca carga")
    print("2) El TIE Advanced x1: velocidad normal, lleva una carga normal")
    print("3) Millennium Falcon: lento, pero lleva mucha carga")

def seleccion_nave():
    while True:
        eleccion_nave = input("Seleccione una con los números respectivos: ")
        if eleccion_nave.isdigit():
            indice_nave = int(eleccion_nave)
            if 1 <= indice_nave <= len(naves): #TODO revisar (usar and?)
                indice_nave -= 1
                nombre_nave = list(naves.keys())[indice_nave]
                print("Seleccionaste la nave", nombre_nave)
                status["nave"]= nombre_nave
                return
            else:
                print("Selección no válida. Por favor, ingresa un número válido para seleccionar una nave.")

def imprimir_datos_nodo(grafo, nodo):
    print("Estás en", nodo)
    descripcion = grafo[nodo].get("description", "Descripción no disponible.")
    print("Descripción:", descripcion)
    print("Llevas recorrido", status["distancia"])
    opciones_disponibles = list(grafo[nodo]["links"].keys())
    print("Sus opciones de viaje son", opciones_disponibles)
    
def seleccionar_destino(grafo):
    while True:
        print("Ingrese el nombre del planeta al que desea viajar:")
        nuevo_destino = input("Destino: ")
        if nuevo_destino in grafo[status["nodo"]]["links"]:
            print("-----------------")
            return nuevo_destino
        else:
            print("Destino no válido. Por favor, ingrese un destino válido. \r\n")

def viajar(grafo, actual, destino,distancia):
    global status
    distancia += float(grafo[actual]["links"][destino] * naves[status["nave"]]["multiplicador"])
    status["distancia"] = distancia
    evento(grafo, destino)

def evento(grafo, destino):
    print(grafo[destino]["evento"]["argumentos"])
    # grafo[destino]["evento"]["funcion"](grafo[destino]["evento"]["argumentos"])
    eval(grafo[destino]["evento"]["funcion"])
    #     evento = grafo[destino]["evento_especial"]
    #     if destino == "Coruscant":
    #         print("Pasaste por Coruscant. El Imperio te confiscó la mitad de tu cargamento. Puedes seguir.")
    #         carga /= 2
    #     elif destino == "Tierra":
    #         print("Has pasado por la Tierra más específicamente por Argentina. Te robaron 20 cargamentos.")
    #         carga -= 20
    #     elif destino == "Jakku":
    #         print("Felicidades llegaste al final de tu ruta espacial. Estos son tus datos")
    #         print("Tu carga total fue de", carga)
            # print("Ruta más corta:", shortest_path)
            # print("Distancia más corta:", shortest_distance)
            
def main():
    global status
    # Inicio
    imprimir_introduccion()
    archivo=seleccionar_archivo()
    grafo = abrir_json(archivo)
    seleccion_nave()
    status["carga"] = naves[status["nave"]]["carga"]
    while True:
        instanciar_nodos(grafo)
        dibujar_grafo(grafo)
        imprimir_datos_nodo(grafo,status["nodo"])
        print("Carga restante:", status["carga"])
        destino = seleccionar_destino(grafo)
        print(status["nodo"])
        viajar(grafo,status["nodo"], destino, status["distancia"])
        status["nodo"] = destino
        print(status["nodo"])

if __name__ == "__main__":
    main()

            
